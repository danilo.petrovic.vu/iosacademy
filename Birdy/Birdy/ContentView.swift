//
//  ContentView.swift
//  Birdy
//
//  Created by student on 13.12.2023..
//

import SwiftUI

struct Tweet: Identifiable {
    var id = UUID()
    let username: String
    var content: String
    let date: Date = Date()
    var isFavorite: Bool
}

struct ContentView: View {
    
    @State var content: String = ""
    @State var isPresented: Bool = false
    @State var username: String = ""
    
    @State var tweets: [Tweet] = [
        Tweet(username: "heda", content: "prvi", isFavorite: true),
        Tweet(username: "heda", content: "drugi", isFavorite: true),
        Tweet(username: "heda", content: "treci", isFavorite: true)
    ]
    
    var body: some View {
        VStack{
            HStack {
                Image(systemName: "bird")
                    .imageScale(.large)
                    .foregroundStyle(.pink)
                Text("Birdy")
                    .font(.title)
                    .foregroundStyle(.pink)
                Spacer()
                if username.isEmpty{
                    Button(action: {isPresented = true}){
                        Text("Log in")
                    }
                } else{
                    Button(action: {username = ""}){
                        Text("Log out")
                    }
                }
                
            }
            List($tweets){ tweet in
                TweetRow(tweet: tweet)
            }
            .listStyle(.plain)
            Spacer()
            if !username.isEmpty{
                HStack{
                    TextField("Content", text: $content)
                    Button(action: {
                        tweets.append(
                            Tweet(username: username, content: content, isFavorite: false))
                    }){
                        Text("Tweet")
                    }
                    .disabled(content.isEmpty)
                }
            }
        }
        
        .padding()
        .sheet(isPresented: $isPresented) {
            LoginView(username: $username, isPresented: $isPresented)
        }
    }
}

#Preview {
    ContentView()
}
